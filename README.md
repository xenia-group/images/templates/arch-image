# Arch base image

This is the base OCI image template that others are created from.

**OS:** Arch Linux

**DE:** N/A

**Init system:** N/A

## Details

`./overlay` - Filesystem overlay

- `overlay/local` - Overlay for the specific image
- `overlay/overlay` - [Global overlay on all images](https://gitlab.com/xenia-group/images/common/overlay)

`./global` - [Global config](https://gitlab.com/xenia-group/images/common/global)

- `fsscript.sh` - Script that runs at end of image creation
- `package.list` - Default package list

`./` - Local settings

- `fsscript.sh` - Script that runs at end of image creation, after global fsscript
- `package.list` - Additional packages for the specific image

